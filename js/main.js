var app = angular.module('app', ['ui.router']);


var adminController = function($scope){
  	$scope.list = data;
    $scope.count = data.length;
  	$scope.propertyName = "";
    $scope.addBoolen = false;
    $scope.editButton = false;

    $scope.form = {
        _id: Date.now(),
        name:"",
        age: null,
        email:"",
        isActive: false,
      }

    // addNew //
    $scope.addNew = function(){
      $scope.list.push($scope.form);
      $scope.count = $scope.list.length;
      $scope.form = "";

      $scope.addBoolen = false;
    }

    // filtr //
    $scope.sortBy = function(name){
      $scope.propertyName = "" + name;
    }

    // delete All Data //
    $scope.deleteData = function(){
      let list = $scope.list;
        $scope.list = [];

        angular.forEach(list, function(val){
          (!val.isActive) ? $scope.list.push(val) : false;
        });
      $scope.count = $scope.list.length;
    }

    // editData //
    $scope.editPost = function(id, value){
      $scope.addBoolen = true;
      $scope.editButton = true;

      angular.forEach($scope.list, function(content){
        if(content._id == id){
          $scope.form.name = content.name;
          $scope.form.email = content.email;
          $scope.form.age = content.age;
          $scope._id = Date.now();
        }
      });
      $scope.list.splice(value, 1);
    }
    // endEdit //
    $scope.endEdit = function(){
      console.log($scope.form._id);
      // angular.forEach($scope.list, function(content){
      //   if(content._id == id){
      //     $scope.form.name = content.name;
      //     $scope.form.email = content.email;
      //     $scope.form.age = content.age;
      //     $scope._id = Date.now();
      //   }
      // });
    }



    // choseAll //
    $scope.chooseAll = function(){
      for(let i in data){
        data[i].isActive = true;
      }
    }
},
  searchController = function($scope,  $http){
    $scope.listShow = true;
    $scope.category = [{
      _id:12,
      name:'учитель',
      state: false,
    },{
      _id:14,
      name:'строитель',
      state: false,
    },{
      _id:15,
      name: 'фитнес',
      state: false,
    }]

    $scope.input = true;

    $scope.result = function(){
      let list = $scope.category;
      $scope.name = [];
      angular.forEach(list, function(val){
        if(val.state == true){
          let nameVal = val.name;
          $scope.name.push(val);
        }
      });

      // вывод инпута //
      angular.forEach($scope.name, function(val){
        if(val){
          $scope.input = false;
        }
      });
      $scope.listShow = true;
    }

    $scope.show = function(){
      $scope.input = true;
      $scope.listShow = false;
    }
  }//closed Controlller//







app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('admin', {
      url: "/",
      templateUrl: "../templates/admin.html",
      controller: adminController,
    })
    .state('input-search', {
      url: "/search",
      templateUrl: "../templates/search.html",
      controller: searchController,
    })

  $urlRouterProvider.otherwise("/");

   
});





















