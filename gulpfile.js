var gulp = require('gulp');
var webserver = require('gulp-webserver'),
	 connect = require('gulp-connect');
 
// gulp.task('server', function() {
//   gulp.src('app')
//     .pipe(webserver({
//     	open: true,
//     	path: '/',
//    		fallback: 'index.html',
//     }));
// });

gulp.task('server', function () {
  connect.server({
    root: './',
    port: 8001,
    livereload: true
  });
});

// gulp.task('watch', function () {
//   gulp.watch(['index.html'], ['html']);
// });